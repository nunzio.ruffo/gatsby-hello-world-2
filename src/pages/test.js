import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

import Saludo from "../components/saludo";

const Test = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Esta es mi pagina de prueba</h1>
    <Saludo />
    
  </Layout>
)

export default Test
